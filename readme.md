
## Guía de trabajo remoto
Ante el último decreto de emergencia publicado por el Estado Peruano, creemos necesario que toda empresa pueda tener un apoyo de cómo implementar teletrabajo, así es que nace esta guía básica para el teletrabajo, en donde recopilamos las herramientas y algunos aprendizajes del trabajo remoto.
Esta guía se encuentra abierta a la comunidad, para ir nutriéndola entre todos de las mejores prácticas, consejos y recomendaciones.
#YoMeQuedoEnCasa #TodosVsCOVID19 #Teletrabajo

**Manifiesto Remoto - Los lineamientos generales**

1. Contratar y trabajar desde cualquier parte del mundo, no desde una ubicación centralizada.
2. Horas de trabajo flexible por sobre los horarios de trabajo fijas.
3. Escribir y grabar el conocimiento/información en vez de sólo las explicaciones verbales.
4. Procesos escritos y detallados en vez de capacitación en el dia a dia.
5. Disponer información de manera pública y continua, no solo cuando sea necesario.
6. Cualquiera puede editar un documento y aportar conocimiento, no importa la jerarquía.
7. La comunicación debe ser hacía varios a la vez y no solo de uno a uno.
8. El resultado del trabajo importa más que las horas puestas.
9. Canales de comunicación formales, oficiales y claros.



**Set de herramientas iniciales**

**Discord**

Permite tener chats de texto y de voz fluidos además de la organización de salas independientes para cada área de tu empresa. Totalmente gratuito.

**Zoom**

Permite crear salas de videoconferencia de hasta 100 personas por un tiempo de 40 minutos (En versión gratuita), además de que el mismo URL de videoconferencia puedes volver a utilizarlo al terminar los 40 minutos, sin bloqueos)

Otras opciones: Meet, hangout, skype.

**Trello**

Permite organizar tableros, que simulan un tipo de pizarra donde colgar las tareas en las que tu y tu equipo van trabajando, de manera transparente. Si recién lo conoces, te recomendamos usar en tus tableros las columnas de &quot;Por hacer&quot;, &quot;En Progreso&quot;, &quot;Hecho&quot; y agregar tareas (tarjetas) en cada una, asignándole a una persona en específico. (La versión gratuita tiene un límite de 10 tableros)

![Imagen modelo KANBAN](https://res.cloudinary.com/openlab-pe/image/upload/v1584335128/Captura.png)

Otras alternativas son: Azana, KanbanFlow, JIRA

**Google Drive y Google Docs**

Permite organizar archivos en la Nube, la versión gratuita te ofrece 5GB de almacenamiento, en el cual puedes crear carpetas, sub carpetas y agregar a tu equipo.

Además, Google Docs, sheets y slides te ayudan para la edición de documentos con varias personas al mismo tiempo y en realtime.

Existen planes en caso requieras de más espacio y otros servicios

- [https://gsuite.google.com/intl/es-419/pricing.html](https://gsuite.google.com/intl/es-419/pricing.html)

Otras alternativas: Office 365 (One drive)

**Google Calendar**

Permite organizar eventos y tareas a través del calendario online de Google, de esta forma evitarás que se pierdan actividades importantes dentro de la rutina diaria.


Sabemos que existen muchas otras herramientas para el trabajo remoto, pero estas son las principales que te recomendamos si estás probando por primera vez esta modalidad.



**Recomendaciones**

**Organizacionales:**

- Organiza bien cada ambiente
  - Discord: Crea un chat de voz y de texto según para cada grupo de tu empresa
  - Zoom: Asegura que todos los trabajadores tengan una cuenta creada
  - Trello: Crea un tablero por cada área pero mantén la visibilidad para todo tu equipo. En caso tu equipo sea menor de 10 personas, usa un único tablero, así todos podrán ver el trabajo y avances de todos.
- Asegura que todos los trabajadores tengan el equipo mínimo en buenas condiciones para operar (Internet, computadora, audífonos, cámara, etc)

**De trabajo conjunto**

- Coordinar horas de conexión
- Mantén tus tareas organizadas tanto individuales como de equipo, usando una herramienta como trello.
- Usa herramientas de recordatorios para tareas específicas como &quot;llamar a proveedor X&quot;, &quot;Enviar email con formato Z&quot;. Para esto existen apps como Wunderlist, Todoist, entre otros.

**Personales**

- Haz un plan con tus tareas pendientes, fíjate plazos y objetivos con un calendario.
- Mantén una rutina:
  - No te despiertes 5 minutos antes de trabajar, ten en cuenta que es un día normal de trabajo, con tu mismo horario de trabajo y cultura.
  - Establece horarios con sus momentos de descanso y alimentación
- Recuerda que estás trabajando, no te distraigas.
- Evita usar tu habitación para el trabajo, busca un espacio libre de distracciones.
- Asegura tu ambiente, a veces necesitamos ciertos objetos para facilitar nuestro trabajo (Monitores adicionales, ventiladores, audífonos)
- No te ahogues con las herramientas, domina lo mínimo para comunicarte y aprende el resto en la marcha.



Recursos adicionales

- Metodología kanban
  - [https://www.apd.es/metodologia-kanban/](https://www.apd.es/metodologia-kanban/)
  - [https://www.youtube.com/watch?v=WP6Nt5XV980](https://www.youtube.com/watch?v=WP6Nt5XV980)
- Cultura organizacional remota de Gitlab, una empresa 100% remota
  - [https://about.gitlab.com/company/culture/all-remote/](https://about.gitlab.com/company/culture/all-remote/)
- Tutorial Discord
  - [https://youtu.be/yM71v8qxFks?t=54](https://youtu.be/yM71v8qxFks?t=54)

Sabemos que existen muchas otras herramientas y recomendaciones. 

Estamos para ayudarnos entre todos. ¿Te gustaría agregar alguna más a esta lista?

Escríbenos y colaboremos: 

- ejdeza@gmail.com 
- evert@starter.pe


Recurso creado por

- Open Lab: https://www.facebook.com/OpenLabPERU
- MVPstarter: http://www.mvpstarter.com
